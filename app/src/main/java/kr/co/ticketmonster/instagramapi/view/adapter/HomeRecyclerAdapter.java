package kr.co.ticketmonster.instagramapi.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import kr.co.ticketmonster.instagramapi.R;
import kr.co.ticketmonster.instagramapi.databinding.ItemHomeListBinding;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import lab.levin.commons.recyclerviews.RecyclerArrayAdapter;
import lombok.Getter;

/**
 * Created by levine on 2017. 2. 14..
 */

public class HomeRecyclerAdapter extends RecyclerArrayAdapter<Feed, HomeRecyclerAdapter.HomeHolder> {


    @Override
    public HomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_home_list, parent, false);

        return new HomeHolder(binding);
    }

    @Override
    public void onBindViewHolder(HomeHolder holder, int position) {
        ItemHomeListBinding binding = holder.getBinding();
        binding.setImage(getItem(position).getImageInfo().getStandardRes());
    }

    @Getter
    public class HomeHolder extends RecyclerView.ViewHolder{

        private ItemHomeListBinding binding;

        public HomeHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            this.binding = (ItemHomeListBinding) viewDataBinding;
        }
    }

    public void appendItems(List<Feed> feeds){
        int currentItemCount = this.getItemCount();
        this.getItems().addAll(feeds);
        notifyItemRangeInserted(currentItemCount, this.getItemCount());
    }
}
