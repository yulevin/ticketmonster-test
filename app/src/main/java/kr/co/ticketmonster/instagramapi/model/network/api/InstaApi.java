package kr.co.ticketmonster.instagramapi.model.network.api;

import kr.co.ticketmonster.instagramapi.model.network.response.json.FeedsResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by levine on 2017. 2. 14..
 */

public interface InstaApi {
    @GET("{user_id}/media")
    Observable<FeedsResponse> findFeedsByUser(@Path("user_id") String userId, @Query("max_id") String maxFeedId);
}
