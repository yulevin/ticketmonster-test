package kr.co.ticketmonster.instagramapi.viewmodel;

import android.content.Intent;
import android.support.annotation.NonNull;

import java.util.List;
import kr.co.ticketmonster.instagramapi.base.BaseViewModel;
import kr.co.ticketmonster.instagramapi.base.Constants;
import kr.co.ticketmonster.instagramapi.model.FeedModel;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import kr.co.ticketmonster.instagramapi.view.DetailActivity;
import kr.co.ticketmonster.instagramapi.view.adapter.DetailRecyclerAdapter;
import ru.noties.debug.Debug;
import rx.observables.ConnectableObservable;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by levine on 2017. 2. 14..
 */

public class DetailViewModel extends BaseViewModel<DetailActivity, FeedModel> {

    private final PublishSubject<String> findIdSubject = PublishSubject.create();

    private FeedModel feedModel;

    private DetailRecyclerAdapter adapter;

    private String currentFindId;
    private String currentMaxId;

    public String navigationTxt;
    private final String NAVI_FORMAT = "%d / %d";

    private boolean nowLoading;
    private int previousTotal;

    public DetailViewModel(DetailActivity view) {
        super(view);
        feedModel = new FeedModel(this);
        setModelWeakReference(feedModel);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        register(compositeSubscription);
    }
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void register(@NonNull CompositeSubscription s) {
        ConnectableObservable<List<Feed>> feedObs = findIdSubject.filter((string) -> string.length() > 1)
                .switchMap(string -> feedModel.getFeedsObs(string, currentMaxId))
                .publish();

        s.add(feedObs
                .doOnNext(feeds -> Debug.d("ViewModel Publishing %d feeds from the ViewModel", feeds.size()))
                .subscribe(feeds -> addList(feeds), throwable -> onError(throwable)));
        s.add(feedObs.connect());
    }

    private void doFind(String findId){
        this.findIdSubject.onNext(findId);
    }

    private void addList(List<Feed> feeds){
        if (null == adapter)
            prepareAdapter();

        setNavigationTxt(getViewWeakReference().getCurrentPosition());
        adapter.appendItems(feeds);
    }

    private void onError(Throwable throwable){
        throwable.printStackTrace();
        getViewWeakReference().getBinding().setViewModel(this);

        register(compositeSubscription);
    }

    public void initData(Intent intent){
        if (null == intent)
            return;

        if (null == adapter)
            prepareAdapter();

        currentFindId = intent.getStringExtra(Constants.BUNDLE_FIND_ID);
        currentMaxId = intent.getStringExtra(Constants.BUNDLE_MAX_ID);

        List<Feed> tempFeeds = intent.getParcelableArrayListExtra(Constants.BUNDLE_FEEDS);
        Feed selectedFeed = intent.getParcelableExtra(Constants.BUNDLE_FEED);
        setSelectedFeed(tempFeeds, selectedFeed);
    }

    private void setSelectedFeed(List<Feed> feeds, Feed feed){
        adapter.addAll(feeds);

        for (int i = 0; i < feeds.size(); i++){
            if (feed.getFeedId().equals(feeds.get(i).getFeedId())){
                getViewWeakReference().setSelectedFeed(i);
                setNavigationTxt(i);
                break;
            }
        }
    }

    private void setNavigationTxt(int pos){
        navigationTxt = String.format(NAVI_FORMAT, pos+1, adapter.getItemCount());
        getViewWeakReference().getBinding().setViewModel(this);
    }

    public void prepareAdapter(){
        adapter = new DetailRecyclerAdapter();
        getViewWeakReference().bindAdapter(adapter);
    }


    public void changeScroll(int totalItemCount, int currentItemPos){
        if (totalItemCount == 0)
            return;
        setNavigationTxt(currentItemPos);

        boolean haveMore = feedModel.isMoreAvailable();

        if (nowLoading) {
            if (totalItemCount > previousTotal) {
                nowLoading = false;
                previousTotal = totalItemCount;
            }
        }

        boolean needLoadMore = haveMore && (totalItemCount==(currentItemPos+1));
        Debug.d("totalItemCount = %d, currentItemPos = %d, haveMore = %s, nowLoading = %s", totalItemCount, currentItemPos, String.valueOf(haveMore), String.valueOf(nowLoading));
        if (!nowLoading && needLoadMore) {
            nowLoading = true;
            Debug.d("load more");
            this.currentMaxId = adapter.getItem(adapter.getItemCount()-1).getFeedId();
            doFind(this.currentFindId);
        }
    }
}
