package kr.co.ticketmonster.instagramapi.base;

import android.support.annotation.NonNull;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by levine on 2017. 2. 18..
 */

public interface RxBind {
    void register(@NonNull CompositeSubscription s);
    void unregister();
}
