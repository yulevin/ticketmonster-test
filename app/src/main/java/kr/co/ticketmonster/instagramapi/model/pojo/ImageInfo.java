package kr.co.ticketmonster.instagramapi.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 14..
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageInfo implements Parcelable{
    @JsonCreator
    public ImageInfo(){

    }

    @JsonProperty("thumbnail")
    public Image thumbnail;

    @JsonProperty("low_resolution")
    public Image lowRes;

    @JsonProperty("standard_resolution")
    public Image standardRes;

    protected ImageInfo(Parcel in) {
        thumbnail = in.readParcelable(Image.class.getClassLoader());
        lowRes = in.readParcelable(Image.class.getClassLoader());
        standardRes = in.readParcelable(Image.class.getClassLoader());
    }

    public static final Creator<ImageInfo> CREATOR = new Creator<ImageInfo>() {
        @Override
        public ImageInfo createFromParcel(Parcel in) {
            return new ImageInfo(in);
        }

        @Override
        public ImageInfo[] newArray(int size) {
            return new ImageInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(thumbnail, i);
        parcel.writeParcelable(lowRes, i);
        parcel.writeParcelable(standardRes, i);
    }
}
