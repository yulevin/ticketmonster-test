package kr.co.ticketmonster.instagramapi.base;

import lab.levin.commons.network.OKHttpClientGenerator;
import lab.levin.commons.network.RetrofitGenerator;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by levine on 2017. 2. 14..
 */

public abstract class BaseRequest {
    protected Retrofit retrofit;
    protected OkHttpClient client;

    protected void initializeRetrofit(String endpoint){
        if (null == retrofit){
            client = OKHttpClientGenerator.getInstance().getOKHttpClient(new OkHttpClient.Builder());
            retrofit = RetrofitGenerator.getInstance().getRetrofit(client, endpoint);
        }
    }

    abstract protected void initialize();
}
