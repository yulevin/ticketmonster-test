package kr.co.ticketmonster.instagramapi.base;

import java.lang.ref.WeakReference;

/**
 * Created by levine on 2017. 2. 15..
 */

public abstract class BaseModel<T extends BaseViewModel> implements LifeCycle {

    private WeakReference<T> viewModelWeakReference;

    public BaseModel(T viewModel){
        setViewModelWeakReference(viewModel);
    };

    public T getViewModelWeakReference() {
        return viewModelWeakReference.get();
    }

    public void setViewModelWeakReference(T viewModel) {
        this.viewModelWeakReference = new WeakReference<T>(viewModel);
    }
}
