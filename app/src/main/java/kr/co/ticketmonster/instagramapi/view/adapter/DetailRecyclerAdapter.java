package kr.co.ticketmonster.instagramapi.view.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import kr.co.ticketmonster.instagramapi.R;
import kr.co.ticketmonster.instagramapi.databinding.ItemDetailListBinding;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import lab.levin.commons.recyclerviews.RecyclerArrayAdapter;
import lombok.Getter;

/**
 * Created by levine on 2017. 2. 14..
 */

public class DetailRecyclerAdapter extends RecyclerArrayAdapter<Feed, DetailRecyclerAdapter.DetailHolder> {

    @Override
    public DetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_detail_list, parent, false);

        return new DetailHolder(binding);
    }

    @Override
    public void onBindViewHolder(DetailHolder holder, int position) {
        ItemDetailListBinding binding = holder.getBinding();
        binding.setImage(getItem(position).getImageInfo().getStandardRes());
    }

    @Getter
    public class DetailHolder extends RecyclerView.ViewHolder {

        private ItemDetailListBinding binding;

        public DetailHolder(ViewDataBinding viewDataBinding) {
            super(viewDataBinding.getRoot());
            this.binding = (ItemDetailListBinding) viewDataBinding;
        }
    }

    public void appendItems(List<Feed> feeds){
        int currentItemCount = this.getItemCount();
        this.getItems().addAll(feeds);
        notifyItemRangeInserted(currentItemCount, this.getItemCount());
    }
}
