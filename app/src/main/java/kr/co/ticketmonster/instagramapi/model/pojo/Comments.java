package kr.co.ticketmonster.instagramapi.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 14..
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comments implements Parcelable{
    @JsonCreator
    public Comments(){

    }

    @JsonProperty("data")
    List<Caption> comments;

    @JsonProperty("count")
    int cnt;

    protected Comments(Parcel in) {
        comments = in.createTypedArrayList(Caption.CREATOR);
        cnt = in.readInt();
    }

    public static final Creator<Comments> CREATOR = new Creator<Comments>() {
        @Override
        public Comments createFromParcel(Parcel in) {
            return new Comments(in);
        }

        @Override
        public Comments[] newArray(int size) {
            return new Comments[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(comments);
        parcel.writeInt(cnt);
    }
}
