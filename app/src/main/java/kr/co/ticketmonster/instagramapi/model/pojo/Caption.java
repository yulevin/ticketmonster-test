package kr.co.ticketmonster.instagramapi.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 14..
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Caption implements Parcelable{
    @JsonCreator
    public Caption() {
    }

    @JsonProperty("id")
    String id;

    @JsonProperty("created_time")
    String createdTime;

    @JsonProperty("text")
    String text;

    @JsonProperty("from")
    User user;

    protected Caption(Parcel in) {
        id = in.readString();
        createdTime = in.readString();
        text = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Caption> CREATOR = new Creator<Caption>() {
        @Override
        public Caption createFromParcel(Parcel in) {
            return new Caption(in);
        }

        @Override
        public Caption[] newArray(int size) {
            return new Caption[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(createdTime);
        parcel.writeString(text);
        parcel.writeParcelable(user, i);
    }
}
