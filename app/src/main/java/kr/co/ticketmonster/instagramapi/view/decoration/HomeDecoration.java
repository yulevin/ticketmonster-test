package kr.co.ticketmonster.instagramapi.view.decoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import lab.levin.commons.utils.DisplayUtils;

/**
 * Created by levine on 2017. 2. 15..
 */

public class HomeDecoration extends RecyclerView.ItemDecoration {


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int pos = parent.getChildAdapterPosition(view);

        if (pos == 0)
            outRect.top = DisplayUtils.getInstance().dpToPixel(view.getContext(), 12);
        else
            outRect.top = DisplayUtils.getInstance().dpToPixel(view.getContext(), 6);

        outRect.left = DisplayUtils.getInstance().dpToPixel(view.getContext(), 0);
        outRect.right = DisplayUtils.getInstance().dpToPixel(view.getContext(), 0);
        outRect.bottom = DisplayUtils.getInstance().dpToPixel(view.getContext(), 6);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
    }
}
