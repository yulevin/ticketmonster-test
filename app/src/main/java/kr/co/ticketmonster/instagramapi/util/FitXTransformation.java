package kr.co.ticketmonster.instagramapi.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import lab.levin.commons.utils.DisplayUtils;

/**
 * Created by levine on 15. 12. 24..
 */
public class FitXTransformation extends BitmapTransformation {


    private Context context;

    public static FitXTransformation getInstance(@NonNull Context context){
        return new FitXTransformation(context);
    }

    private FitXTransformation(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return bitmapChanger(toTransform);
    }

    @Override
    public String getId() {
        return "FitTransformation@" + hashCode();
    }

    public Bitmap bitmapChanger(Bitmap bitmap) {
        float originalWidth = bitmap.getWidth();
        float originalHeight = bitmap.getHeight();

        float scale = DisplayUtils.getInstance().deviceWidth(context) / originalWidth;

        return Bitmap.createScaledBitmap(bitmap, DisplayUtils.getInstance().deviceWidth(context), (int)(originalHeight*scale), true);
    }
}
