package kr.co.ticketmonster.instagramapi.base;


import java.lang.ref.WeakReference;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by levine on 2017. 2. 14..
 */

public abstract class BaseViewModel<BaseView, T extends BaseModel> implements LifeCycle, RxBind {

    protected CompositeSubscription compositeSubscription;
    private WeakReference<BaseView> viewWeakReference;
    private WeakReference<T> modelWeakReference;

    public BaseViewModel(BaseView view){
        setViewWeakReference(view);
    };

    @Override
    public void onCreate() {
        if (null == compositeSubscription)
            compositeSubscription = new CompositeSubscription();

        if (null != modelWeakReference.get())
            modelWeakReference.get().onCreate();
    }

    @Override
    public void onPause() {
        if (null != modelWeakReference.get())
            modelWeakReference.get().onPause();
    }

    @Override
    public void onResume() {
        if (null != modelWeakReference.get())
            modelWeakReference.get().onResume();

    }

    @Override
    public void onStop() {
        if (null != modelWeakReference.get())
            modelWeakReference.get().onStop();

    }

    @Override
    public void onDestroy() {
        if (null != modelWeakReference.get())
            modelWeakReference.get().onDestroy();

        unregister();
    }

    public BaseView getViewWeakReference() {
        return viewWeakReference.get();
    }

    public void setViewWeakReference(BaseView view) {
        this.viewWeakReference = new WeakReference<BaseView>(view);
    }

    public T getModelWeakReference() {
        return modelWeakReference.get();
    }

    public void setModelWeakReference(T model) {
        this.modelWeakReference = new WeakReference<T>(model);
    }

    @Override
    public void unregister(){
        if (null != compositeSubscription && compositeSubscription.hasSubscriptions()){
            compositeSubscription.clear();
            compositeSubscription = null;
        }
    };
}
