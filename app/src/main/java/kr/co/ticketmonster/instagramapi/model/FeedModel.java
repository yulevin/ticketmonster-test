package kr.co.ticketmonster.instagramapi.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.List;

import kr.co.ticketmonster.instagramapi.base.BaseModel;
import kr.co.ticketmonster.instagramapi.base.BaseViewModel;
import kr.co.ticketmonster.instagramapi.model.network.request.InstaApiRequest;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import ru.noties.debug.Debug;
import rx.Observable;

/**
 * Created by levine on 2017. 2. 15..
 */

public class FeedModel extends BaseModel {

    private boolean moreAvailable = true;

    public FeedModel(BaseViewModel viewModel) {
        super(viewModel);
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    public Observable<List<Feed>> getFeedsObs(@NonNull String findId, @Nullable String maxId){
        Debug.e("userId = %s", findId);
        Debug.e("maxId = %s", TextUtils.isEmpty(maxId) ? "It's empty" : maxId);
        return InstaApiRequest.getInstance().getFeeds(findId, maxId)
                .doOnNext(response -> {
                    moreAvailable = response.isMoreAvailable();
                    Debug.d("moreAvailable = %s", String.valueOf(moreAvailable));
                })
                .filter(feedsResponse -> !feedsResponse.isEmpty())
                .map(response -> response.getFeeds());
    }

    public boolean isMoreAvailable() {
        return moreAvailable;
    }
}
