package kr.co.ticketmonster.instagramapi.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 13..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Feed implements Parcelable{

    private final String IMAGE = "image";
    private final String VIDEO = "video";

    @JsonCreator
    public Feed() {
    }

    @JsonProperty("link")
    String link;

    @JsonProperty("type")
    String type;

    @JsonProperty("caption")
    Caption caption;

    @JsonProperty("can_delete_comments")
    boolean canDelComments;

    @JsonProperty("user")
    User user;

    @JsonProperty("code")
    String code;

    @JsonProperty("id")
    String feedId;

    @JsonProperty("can_view_comments")
    boolean canViewComments;

    @JsonProperty("likes")
    Like likes;

    @JsonProperty("images")
    public ImageInfo imageInfo;

    @JsonProperty("videos")
    public ImageInfo videoInfo;

    @JsonProperty("location")
    Location location;

    @JsonProperty("comments")
    Comments comments;

    @JsonProperty("alt_media_url")
    String altMediaUrl;

    @JsonProperty("created_time")
    String createdTime;

    @JsonProperty("video_views")
    int videoViews;

    protected Feed(Parcel in) {
        link = in.readString();
        type = in.readString();
        caption = in.readParcelable(Caption.class.getClassLoader());
        canDelComments = in.readByte() != 0;
        user = in.readParcelable(User.class.getClassLoader());
        code = in.readString();
        feedId = in.readString();
        canViewComments = in.readByte() != 0;
        likes = in.readParcelable(Like.class.getClassLoader());
        imageInfo = in.readParcelable(ImageInfo.class.getClassLoader());
        videoInfo = in.readParcelable(ImageInfo.class.getClassLoader());
        location = in.readParcelable(Location.class.getClassLoader());
        comments = in.readParcelable(Comments.class.getClassLoader());
        altMediaUrl = in.readString();
        createdTime = in.readString();
        videoViews = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(link);
        dest.writeString(type);
        dest.writeParcelable(caption, flags);
        dest.writeByte((byte) (canDelComments ? 1 : 0));
        dest.writeParcelable(user, flags);
        dest.writeString(code);
        dest.writeString(feedId);
        dest.writeByte((byte) (canViewComments ? 1 : 0));
        dest.writeParcelable(likes, flags);
        dest.writeParcelable(imageInfo, flags);
        dest.writeParcelable(videoInfo, flags);
        dest.writeParcelable(location, flags);
        dest.writeParcelable(comments, flags);
        dest.writeString(altMediaUrl);
        dest.writeString(createdTime);
        dest.writeInt(videoViews);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Feed> CREATOR = new Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };

    public boolean isImage(){
        return this.IMAGE.equals(this.type);
    }

}
