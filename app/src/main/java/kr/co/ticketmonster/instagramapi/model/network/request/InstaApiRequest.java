package kr.co.ticketmonster.instagramapi.model.network.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import kr.co.ticketmonster.instagramapi.base.BaseRequest;
import kr.co.ticketmonster.instagramapi.model.network.api.InstaApi;
import kr.co.ticketmonster.instagramapi.model.network.response.json.FeedsResponse;
import ru.noties.debug.Debug;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by levine on 2017. 2. 14..
 */

public class InstaApiRequest extends BaseRequest {

    private InstaApi api;
    private final String END_POINT = "https://www.instagram.com/";

    public static InstaApiRequest getInstance() {
        return Singleton.instance;
    }

    private InstaApiRequest() {
        initialize();
    }

    @Override
    protected void initialize() {
        initializeRetrofit(END_POINT);
        if (null == api)
            api = retrofit.create(InstaApi.class);
    }

    private static class Singleton {
        private static final InstaApiRequest instance = new InstaApiRequest();
    }

    public Observable<FeedsResponse> getFeeds(@NonNull String userId, @Nullable String maxFeedId){
        Debug.e("userId = %s", userId);
        return api.findFeedsByUser(userId, maxFeedId).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }
}
