package kr.co.ticketmonster.instagramapi;

import android.support.multidex.MultiDexApplication;

import ru.noties.debug.Debug;

/**
 * Created by levine on 2017. 2. 16..
 */

public class Apps extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        Debug.init(BuildConfig.DEBUG);
    }
}
