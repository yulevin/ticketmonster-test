package kr.co.ticketmonster.instagramapi.base;

public interface LifeCycle {

    void onCreate();
    void onPause();
    void onResume();
    void onStop();
    void onDestroy();

}
