package kr.co.ticketmonster.instagramapi.base;

/**
 * Created by levine on 2017. 2. 17..
 */

public interface Constants {
    public static final String BUNDLE_FEED = "feed";
    public static final String BUNDLE_FEEDS = "feeds";
    public static final String BUNDLE_MAX_ID = "max_id";
    public static final String BUNDLE_FIND_ID = "find_id";
}
