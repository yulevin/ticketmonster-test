package kr.co.ticketmonster.instagramapi.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 14..
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Parcelable{
    @JsonCreator
    public User() {
    }

    @JsonProperty("full_name")
    String fullName;

    @JsonProperty("id")
    String id;

    @JsonProperty("profile_picture")
    String profilePic;

    @JsonProperty("username")
    String userName;

    protected User(Parcel in) {
        fullName = in.readString();
        id = in.readString();
        profilePic = in.readString();
        userName = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fullName);
        parcel.writeString(id);
        parcel.writeString(profilePic);
        parcel.writeString(userName);
    }
}
