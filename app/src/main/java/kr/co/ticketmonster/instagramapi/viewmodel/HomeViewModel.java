package kr.co.ticketmonster.instagramapi.viewmodel;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import kr.co.ticketmonster.instagramapi.R;
import kr.co.ticketmonster.instagramapi.base.BaseViewModel;
import kr.co.ticketmonster.instagramapi.base.Constants;
import kr.co.ticketmonster.instagramapi.model.FeedModel;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import kr.co.ticketmonster.instagramapi.view.DetailActivity;
import kr.co.ticketmonster.instagramapi.view.HomeActivity;
import kr.co.ticketmonster.instagramapi.view.adapter.HomeRecyclerAdapter;
import lab.levin.commons.utils.DisplayUtils;
import ru.noties.debug.Debug;
import rx.observables.ConnectableObservable;
import rx.schedulers.TestScheduler;
import rx.subjects.PublishSubject;
import rx.subjects.TestSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by levine on 2017. 2. 14..
 */

public class HomeViewModel extends BaseViewModel<HomeActivity, FeedModel>{

    private final PublishSubject<String> findIdSubject = PublishSubject.create();

    @VisibleForTesting
    private final TestSubject<String> testSubject = TestSubject.create(new TestScheduler());

    private final int FIND_INPUT_DELAY = 300;

    private FeedModel feedModel;

    private HomeRecyclerAdapter adapter;
    private String currentFindId;
    private String previousFindId;
    private String currentMaxId;
    private boolean nowLoading;
    private int previousTotal;

    public String message;
    public boolean isEmpty;

    public HomeViewModel(HomeActivity view) {
        super(view);
        feedModel = new FeedModel(this);
        setModelWeakReference(feedModel);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        currentMaxId = "";
        currentFindId = "";
        previousFindId = "";
        nowLoading = false;

        register(compositeSubscription);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void register(@NonNull CompositeSubscription s) {
        ConnectableObservable<List<Feed>> feedObs = findIdSubject
                .filter(string -> !TextUtils.isEmpty(string))
                .map(string -> chkChangeFindId(string))
                .switchMap(string -> feedModel.getFeedsObs(string, currentMaxId))
                .publish();

        s.add(feedObs
                .doOnNext(feeds -> setCurrentMaxId(feeds.get(feeds.size()-1).getFeedId()))
                .doOnNext(feeds -> Debug.d("ViewModel Publishing %d feeds from the ViewModel", feeds.size()))
                .subscribe(feeds -> addList(feeds), throwable -> onError(throwable)));
        s.add(feedObs.connect());
    }

    private String chkChangeFindId(@NonNull String id){
        if (!previousFindId.equals(id))
            setCurrentMaxId("");

        if (previousFindId.length() > id.length() && 2 > id.length())
            id = " ";

        return id;
    }


    private void addList(List<Feed> feeds){
        if (null == adapter)
            prepareAdapter();

        //add defence code
        if ((null == feeds || 1 > feeds.size()) && 1 > previousTotal){
            onError(null);
            return;
        }

        //result is changed
        if (!currentFindId.equals(previousFindId))
            adapter.clear();

        previousFindId = currentFindId;

        isEmpty = false;
        getViewWeakReference().getBinding().setViewModel(this);
        adapter.appendItems(feeds);
    }

    private void onError(Throwable throwable){
        throwable.printStackTrace();
        isEmpty = true;
        message = getViewWeakReference().getString(R.string.msg_empty);
        getViewWeakReference().getBinding().setViewModel(this);

        register(compositeSubscription);
    }

    public void prepareAdapter(){
        adapter = new HomeRecyclerAdapter();
        getViewWeakReference().bindAdapter(adapter);
    }

    public void generateIntent(int pos){
        Intent intent = new Intent(getViewWeakReference(), DetailActivity.class);
        intent.putParcelableArrayListExtra(Constants.BUNDLE_FEEDS, adapter.getItems());
        intent.putExtra(Constants.BUNDLE_FEED, adapter.getItem(pos));
        intent.putExtra(Constants.BUNDLE_MAX_ID, currentMaxId);
        intent.putExtra(Constants.BUNDLE_FIND_ID, currentFindId);

        getViewWeakReference().moveToDetail(intent);
    }

    public void loadMore(int visibleItemCount, int totalItemCount, int firstVisibleItem){
        if (totalItemCount == 0)
            return;

        boolean haveMore = feedModel.isMoreAvailable();

        if (nowLoading) {
            if (totalItemCount > previousTotal) {
                nowLoading = false;
                previousTotal = totalItemCount;
            }
        }

        boolean needLoadMore = haveMore && (totalItemCount - visibleItemCount) <= (firstVisibleItem);
        if (!nowLoading && needLoadMore) {
            nowLoading = true;
            Debug.d("load more");
            setFindId(this.currentFindId);
        }
    }

    public void setFindId(@NonNull String id){
        Debug.e("userId = %s", id);
        this.currentFindId = id;
        this.findIdSubject.onNext(id);
    }

    public void setCurrentMaxId(@NonNull String id){
        this.currentMaxId = id;
    }
}
