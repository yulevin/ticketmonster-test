package kr.co.ticketmonster.instagramapi.util;

import com.bumptech.glide.request.RequestListener;

import ru.noties.debug.Debug;

/**
 * Created by levine on 15. 12. 27..
 */
public class LoggingListener<T, R> implements RequestListener<T, R> {

    @Override
    public boolean onException(Exception e, T model, com.bumptech.glide.request.target.Target<R> target, boolean isFirstResource) {
        if (null != e)
            e.printStackTrace();

        Debug.e("onException(%s, %s, %s, %s)", (null != e) ? e.getLocalizedMessage() : "null", model, target, isFirstResource);
        return true;
    }

    @Override
    public boolean onResourceReady(R resource, T model, com.bumptech.glide.request.target.Target<R> target, boolean isFromMemoryCache, boolean isFirstResource) {
        target.onResourceReady(resource, null);
        Debug.d("onResourceReady(%s, %s, %s, %s, %s)", resource, model, target, isFromMemoryCache, isFirstResource);
        return true;
    }
}