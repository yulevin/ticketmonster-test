package kr.co.ticketmonster.instagramapi.base;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import java.lang.ref.WeakReference;

import lab.levin.commons.utils.DisplayUtils;
import qiu.niorgai.StatusBarCompat;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by levine on 2017. 2. 15..
 */

public abstract class BaseActivity<ViewModel extends BaseViewModel> extends AppCompatActivity implements BaseView, RxBind {

    private WeakReference<ViewModel> viewModelWeakReference;
    protected CompositeSubscription compositeSubscription;
    private boolean keyboardListenersAttached = false;

    private boolean isShowKeyboard;


    abstract protected void initViewModel();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (null == compositeSubscription)
            compositeSubscription = new CompositeSubscription();

        initViewModel();
        if (null != viewModelWeakReference.get())
            viewModelWeakReference.get().onCreate();
        super.onCreate(savedInstanceState);
        attachKeyboardListeners();
    }

    @Override
    protected void onResume() {
        if (null != viewModelWeakReference.get())
            viewModelWeakReference.get().onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (null != viewModelWeakReference.get())
            viewModelWeakReference.get().onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (null != viewModelWeakReference.get())
            viewModelWeakReference.get().onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (null != viewModelWeakReference.get())
            viewModelWeakReference.get().onDestroy();

        if (keyboardListenersAttached)
            getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(keyboardLayoutListener);

        unregister();
        super.onDestroy();
    }

    @Override
    public void unregister(){
        if (null != compositeSubscription && compositeSubscription.hasSubscriptions()){
            compositeSubscription.clear();
            compositeSubscription = null;
        }
    };

    /**
     * must be called after setContentView
     */
    protected void setSystembarTint(boolean hideStatusBarBackground){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //should hide status bar background (default black background) when SDK >= 21
            StatusBarCompat.translucentStatusBar(this, hideStatusBarBackground);
        } else {
            //translucent status bar
            StatusBarCompat.translucentStatusBar(this);
        }
    }

    public ViewModel getViewModelWeakReference() {
        return viewModelWeakReference.get();
    }

    public void setViewModelWeakReference(ViewModel viewModel) {
        this.viewModelWeakReference = new WeakReference<ViewModel>(viewModel);
    }


    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            Rect r = new Rect();
            getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
            int screenHeight = getWindow().getDecorView().getRootView().getHeight();

            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            // 0.15 ratio is perhaps enough to determine keypad height.
            isShowKeyboard = (keypadHeight > screenHeight * 0.15);
        }
    };

    protected void attachKeyboardListeners() {
        if (keyboardListenersAttached)
            return;

        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);

        keyboardListenersAttached = true;
    }

    public boolean isShowKeyboard() {
        return isShowKeyboard;
    }
}
