package kr.co.ticketmonster.instagramapi.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.inputmethod.EditorInfo;

import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import kr.co.ticketmonster.instagramapi.R;
import kr.co.ticketmonster.instagramapi.base.BaseActivity;
import kr.co.ticketmonster.instagramapi.databinding.ActivityHomeBinding;
import kr.co.ticketmonster.instagramapi.view.adapter.HomeRecyclerAdapter;
import kr.co.ticketmonster.instagramapi.view.decoration.HomeDecoration;
import kr.co.ticketmonster.instagramapi.viewmodel.HomeViewModel;
import lab.levin.commons.recyclerviews.RecyclerItemClickListener;
import lab.levin.commons.utils.DisplayUtils;
import ru.noties.debug.Debug;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public class HomeActivity extends BaseActivity<HomeViewModel> {

    private ActivityHomeBinding binding;
    private Observable<String> findIdObs;
    private Observable<String> findKeyObs;
    private Observable<String> findBtnObs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        binding.setViewModel(getViewModelWeakReference());

        prepareViews();
    }

    @Override
    protected void initViewModel() {
        setViewModelWeakReference(new HomeViewModel(this));
    }

    @Override
    public void register(@NonNull CompositeSubscription s) {
        s.add(findIdObs.observeOn(AndroidSchedulers.mainThread()).subscribe(str -> getViewModelWeakReference().setFindId(str)));
        s.add(findKeyObs.observeOn(AndroidSchedulers.mainThread()).subscribe(str -> getViewModelWeakReference().setFindId(str)));
        s.add(findBtnObs.observeOn(AndroidSchedulers.mainThread()).subscribe(str -> getViewModelWeakReference().setFindId(str)));
    }

    @Override
    protected void onStop() {
        super.onStop();
        DisplayUtils.getInstance().hideKeyboard(this, binding.etFind);
    }

    public ActivityHomeBinding getBinding() {
        return binding;
    }

    private void prepareViews(){
        binding.homeRecycler.setLayoutManager(new LinearLayoutManager(this));
        binding.homeRecycler.addItemDecoration(new HomeDecoration());
        getViewModelWeakReference().prepareAdapter();

        binding.homeRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = recyclerView.getAdapter().getItemCount();
                int firstVisibleItem = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                getViewModelWeakReference().loadMore(visibleItemCount, totalItemCount, firstVisibleItem);
            }
        });

        binding.homeRecycler.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        this,
                        (view, position) -> {
                            if (isShowKeyboard()){
                                DisplayUtils.getInstance().hideKeyboard(this, binding.etFind);
                                return;
                            }
                            getViewModelWeakReference().generateIntent(position);
                        }
                ));

        findIdObs = RxTextView.textChanges(binding.etFind)
                .map(charSequence -> charSequence.toString());

        findKeyObs = RxTextView.editorActionEvents(binding.etFind)
                .filter(textViewEditorActionEvent -> EditorInfo.IME_ACTION_SEARCH == textViewEditorActionEvent.actionId())
                .map(aVoid -> binding.etFind.getText().toString());

        findBtnObs = RxView.clicks(binding.btnFind)
                .map(aVoid -> binding.etFind.getText().toString());

        this.register(compositeSubscription);
    }

    public void moveToDetail(Intent intent){
        startActivity(intent);
    }

    public void bindAdapter(HomeRecyclerAdapter adapter){
        binding.homeRecycler.setAdapter(adapter);
    }
}
