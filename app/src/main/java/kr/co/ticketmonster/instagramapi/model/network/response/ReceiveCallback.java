package kr.co.ticketmonster.instagramapi.model.network.response;

/**
 * Created by levine on 2017. 2. 14..
 */

public interface ReceiveCallback<T> {
    void onReceive(T t);
}
