package kr.co.ticketmonster.instagramapi.model.network.response.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 14..
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedsResponse {
    @JsonCreator
    public FeedsResponse() {
    }

    @JsonProperty("more_available")
    boolean moreAvailable;

    @JsonProperty("items")
    List<Feed> feeds;

    @JsonProperty("status")
    String status;

    public boolean isEmpty(){
        return null == feeds ? true : (1 > feeds.size());
    }
}
