package kr.co.ticketmonster.instagramapi.util;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by Levine on 2017. 2. 14..
 */

public class DataBindingAdapter {
    @BindingAdapter("safetyText")
    public static void setSafetyText(TextView textView, String text){
        textView.setText(TextUtils.isEmpty(text) ? "" : text);
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .crossFade()
//                .listener(new LoggingListener<String, GlideDrawable>())
                .bitmapTransform(FitXTransformation.getInstance(imageView.getContext()))
                .into(imageView);
    }

    @BindingAdapter({"imageUrl", "placeHolder"})
    public static void setImageUrl(ImageView imageView, String url, Drawable placeHolder) {
        Glide.with(imageView.getContext())
                .load(url)
                .crossFade()
                .placeholder(placeHolder)
                .error(placeHolder)
//                .listener(new LoggingListener<String, GlideDrawable>())
                .bitmapTransform(FitXTransformation.getInstance(imageView.getContext()))
                .into(imageView);
    }
}

