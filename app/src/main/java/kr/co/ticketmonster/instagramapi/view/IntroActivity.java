package kr.co.ticketmonster.instagramapi.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import java.lang.ref.WeakReference;

import kr.co.ticketmonster.instagramapi.R;
import kr.co.ticketmonster.instagramapi.base.BaseActivity;
import kr.co.ticketmonster.instagramapi.databinding.ActivityIntroBinding;
import qiu.niorgai.StatusBarCompat;
import rx.subscriptions.CompositeSubscription;

public class IntroActivity extends AppCompatActivity {

    private ActivityIntroBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_intro);
        StatusBarCompat.translucentStatusBar(this, true);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            Transition fade = new Fade()
                    .setDuration(600)
                    .addTarget(binding.ivLogo)
                    .addListener(new Transition.TransitionListener() {
                        @Override
                        public void onTransitionStart(Transition transition) {

                        }

                        @Override
                        public void onTransitionEnd(Transition transition) {
                            moveToHome();
                        }

                        @Override
                        public void onTransitionCancel(Transition transition) {

                        }

                        @Override
                        public void onTransitionPause(Transition transition) {

                        }

                        @Override
                        public void onTransitionResume(Transition transition) {

                        }
                    });

            TransitionManager.beginDelayedTransition(binding.activityIntro, fade);
            binding.ivLogo.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        handler.sendEmptyMessageDelayed(0, 1000);
    }

    @Override
    public void onStop() {
        super.onStop();
        handler.removeMessages(0);
    }

    private void moveToHome(){
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
