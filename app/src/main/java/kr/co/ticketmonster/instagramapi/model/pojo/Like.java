package kr.co.ticketmonster.instagramapi.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by levine on 2017. 2. 14..
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Like implements Parcelable{
    @JsonCreator
    public Like(){

    }

    @JsonProperty("data")
    List<User> likeUsers;

    @JsonProperty("count")
    int cnt;

    protected Like(Parcel in) {
        likeUsers = in.createTypedArrayList(User.CREATOR);
        cnt = in.readInt();
    }

    public static final Creator<Like> CREATOR = new Creator<Like>() {
        @Override
        public Like createFromParcel(Parcel in) {
            return new Like(in);
        }

        @Override
        public Like[] newArray(int size) {
            return new Like[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(likeUsers);
        parcel.writeInt(cnt);
    }
}
