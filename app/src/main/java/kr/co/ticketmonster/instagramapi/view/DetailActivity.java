package kr.co.ticketmonster.instagramapi.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import kr.co.ticketmonster.instagramapi.R;
import kr.co.ticketmonster.instagramapi.base.BaseActivity;
import kr.co.ticketmonster.instagramapi.databinding.ActivityDetailBinding;
import kr.co.ticketmonster.instagramapi.view.adapter.DetailRecyclerAdapter;
import kr.co.ticketmonster.instagramapi.viewmodel.DetailViewModel;
import rx.subscriptions.CompositeSubscription;

public class DetailActivity extends BaseActivity<DetailViewModel> {

    private ActivityDetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.setViewModel(getViewModelWeakReference());
        setSystembarTint(true);

        prepareViews();
        getViewModelWeakReference().initData(getIntent());
    }

    @Override
    protected void initViewModel() {
        setViewModelWeakReference(new DetailViewModel(this));
    }

    @Override
    public void register(@NonNull CompositeSubscription s) { }

    public ActivityDetailBinding getBinding() {
        return binding;
    }

    private void prepareViews(){
        binding.detailRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        getViewModelWeakReference().prepareAdapter();
        binding.detailRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                int totalItemCount = recyclerView.getAdapter().getItemCount();
                int currentPos = binding.detailRecycler.getCurrentPosition();
                getViewModelWeakReference().changeScroll(totalItemCount, currentPos);
            }
        });
    }

    public void bindAdapter(DetailRecyclerAdapter adapter){
        binding.detailRecycler.setAdapter(adapter);
    }

    public void setSelectedFeed(int pos){
        binding.detailRecycler.scrollToPosition(pos);
    }

    public int getCurrentPosition(){
        return binding.detailRecycler.getCurrentPosition();
    }
}
