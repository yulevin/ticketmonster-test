package kr.co.ticketmonster.instagramapi.model;

import android.text.TextUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import kr.co.ticketmonster.instagramapi.RxJavaTestRunner;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import kr.co.ticketmonster.instagramapi.viewmodel.HomeViewModel;
import rx.Observable;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by levine on 2017. 2. 20..
 */
@RunWith(RxJavaTestRunner.class)
public class FeedModelTest {
    private FeedModel feedModel;
    private String[] findIds;

    private String maxId;

    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook(){
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
        feedModel = new FeedModel(mock(HomeViewModel.class));

        findIds = new String[]{
                "angelinadanilova",
                "uhan_sol"
        };

        maxId = "";
    }

    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.getInstance().reset();
    }

    @Test
    public void test_feedModel_getObs() throws Exception {
        Observable<List<Feed>> obs1 = feedModel.getFeedsObs(findIds[0], maxId);
        Observable<List<Feed>> obs2 = feedModel.getFeedsObs(findIds[1], maxId);

        TestSubscriber<List<Feed>> testSubscriber1 = new TestSubscriber<>();
        TestSubscriber<List<Feed>> testSubscriber2 = new TestSubscriber<>();

        obs1.subscribe(testSubscriber1);
        obs2.subscribe(testSubscriber2);

        testSubscriber1.assertNoErrors();
        testSubscriber2.assertNoErrors();

        List<List<Feed>> feeds1 = testSubscriber1.getOnNextEvents();
        List<List<Feed>> feeds2 = testSubscriber2.getOnNextEvents();

//        .filter(elements -> null != elements && elements.size() > 0)
//                .filter(elements -> null != elements && elements.size() > 0)
        feeds1.parallelStream().forEach(feeds -> getElement(feeds));
        feeds2.parallelStream().forEach(feeds -> getElement(feeds));
    }

    private void getElement(List<Feed> arr){
        arr.parallelStream().forEach(feed -> assertNotNull("Checking feed is null", feed));
    }
}