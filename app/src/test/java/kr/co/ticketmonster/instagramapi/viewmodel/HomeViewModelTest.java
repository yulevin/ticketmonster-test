package kr.co.ticketmonster.instagramapi.viewmodel;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.List;
import java.util.concurrent.TimeUnit;

import kr.co.ticketmonster.instagramapi.RxJavaTestRunner;
import kr.co.ticketmonster.instagramapi.model.FeedModel;
import kr.co.ticketmonster.instagramapi.model.pojo.Feed;
import kr.co.ticketmonster.instagramapi.view.HomeActivity;
import ru.noties.debug.Debug;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import rx.schedulers.TestScheduler;
import rx.subjects.TestSubject;
import rx.subscriptions.CompositeSubscription;

import static org.junit.Assert.*;

/**
 * Created by levine on 2017. 2. 19..
 */
@RunWith(RxJavaTestRunner.class)
public class HomeViewModelTest {

    private HomeViewModel viewModel;
    private FeedModel feedModel;

    private String prevMaxId;
    private String prevFindId;
    private String currentFindId;
    private String currentMaxId;

    private final int FIND_INPUT_DELAY = 0;
    private TestScheduler scheduler;
    private TestSubject<String> subject;
    private CompositeSubscription s;

    private int previousTotal;

    @Mock
    HomeActivity activity;


    @Before
    public void setUp() throws Exception {
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook(){
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });

        prevMaxId = "";
        prevFindId = "";
        scheduler = new TestScheduler();
        subject = TestSubject.create(scheduler);
        s = new CompositeSubscription();

        viewModel = new HomeViewModel(activity);
        viewModel.onCreate();
        feedModel = new FeedModel(viewModel);

        register(s, subject);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void test_homeViewModel_setFindId(){
        setCurrentFindId("levin_yp", 0);
        //must be find id and max id change
        setCurrentFindId("angelinadanilova", 5000);

        setCurrentFindId("angelinadanilova", 10000);
    }

    private void register(CompositeSubscription s, TestSubject<String> subject){

        ConnectableObservable<List<Feed>> feedObs = subject
                .filter(string -> !TextUtils.isEmpty(string))
                .debounce(FIND_INPUT_DELAY, TimeUnit.MILLISECONDS)
                .map(string -> chkChangeFindId(string))
                .switchMap(string -> feedModel.getFeedsObs(string, currentMaxId))
                .publish();

        s.add(feedObs
                .doOnNext(feeds -> setCurrentMaxId(feeds.get(feeds.size()-1).getFeedId()))
                .doOnNext(feeds -> Debug.d("ViewModel Publishing %d feeds from the ViewModel", feeds.size()))
                .subscribe(feeds -> {

                    System.out.println(String.format("ViewModel Publishing %d feeds from the ViewModel", feeds.size()));
                    setPreviousTotal(feeds.size());

                    assertNotNull("Checking current maxId", this.getCurrentMaxId());
                    assertNotNull("Checking current findId", this.getCurrentFindId());
                    assertNotEquals("Checking changed maxId", prevMaxId, this.getCurrentMaxId());
                    assertNotEquals("Checking changed findId", prevFindId, this.getCurrentFindId());

                    assertThat("Checking previus total feeds count", this.getPreviousTotal(), is(0));

                    prevMaxId = this.getCurrentMaxId();
                    prevFindId = this.getCurrentFindId();

                    System.out.println("prevMaxId : " + this.getPrevMaxId());
                    System.out.println("currentMaxId : " + this.getCurrentMaxId());
                    System.out.println("prevFindId : " + this.getPrevFindId());
                    System.out.println("currentMaxId" + this.getCurrentMaxId());
                    System.out.println("previousTotal" + this.getPreviousTotal());

                }, throwable -> throwable.printStackTrace()
                ));

        s.add(feedObs.connect());
    }

    private String chkChangeFindId(@NonNull String id){
        if (!prevFindId.equals(id))
            setCurrentMaxId("");

        if (prevFindId.length() > id.length() && 2 > id.length())
            id = " ";

        return id;
    }

    public String getPrevMaxId() {
        return prevMaxId;
    }

    public void setPrevMaxId(String prevMaxId) {
        this.prevMaxId = prevMaxId;
    }

    public String getPrevFindId() {
        return prevFindId;
    }

    public void setPrevFindId(String prevFindId) {
        this.prevFindId = prevFindId;
    }

    public String getCurrentFindId() {
        return currentFindId;
    }

    public void setCurrentFindId(String currentFindId, long delayTime) {
        this.currentFindId = currentFindId;
        this.subject.onNext(currentFindId, delayTime);
    }

    public String getCurrentMaxId() {
        return currentMaxId;
    }

    public void setCurrentMaxId(String currentMaxId) {
        this.currentMaxId = currentMaxId;
    }

    public int getPreviousTotal() {
        return previousTotal;
    }

    public void setPreviousTotal(int previousTotal) {
        this.previousTotal += previousTotal;
    }
}