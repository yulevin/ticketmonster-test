package kr.co.ticketmonster.instagramapi;


import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;

import rx.Scheduler;
import rx.plugins.RxJavaPlugins;
import rx.plugins.RxJavaSchedulersHook;
import rx.schedulers.Schedulers;

/**
 * Created by levine on 2017. 2. 20..
 */

public class RxJavaTestRunner extends MockitoJUnitRunner {
    public RxJavaTestRunner(Class<?> klass) throws InvocationTargetException {
        super(klass);

        RxJavaPlugins.getInstance().reset();
        RxJavaPlugins.getInstance().registerSchedulersHook(new RxJavaSchedulersHook(){
            @Override
            public Scheduler getIOScheduler() {
                return Schedulers.immediate();
            }
        });
    }
}
